public abstract class Phone {
    String name;
    String model;
    int memorySize;
    int sizeRAM;

    void setName(String phoneName){
        name=phoneName;
    }
    String getName(){
        return name;
    }
    void setModel(String phoneModel){
        model=phoneModel;
    }
    String getModel(){
        return model;
    }
    void setMemorySize(int sizeOfThePhoneMemory){
        memorySize=sizeOfThePhoneMemory;
    }
    int getMemorySize(){
        return memorySize;
    }
    void setSizeRAM(int sizeOfThePhoneRAM){
        sizeRAM=sizeOfThePhoneRAM;
    }
    int getSizeRAM(){
        return sizeRAM;
    }
}
