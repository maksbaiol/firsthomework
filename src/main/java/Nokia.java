public class Nokia extends Phone implements PhoneConnection {

    @Override
    public void dial() {
        System.out.println("Calling to a Sam!");
    }

    @Override
    public void sendMassage() {
        System.out.println("Sending a massage to Sam");
    }
}
