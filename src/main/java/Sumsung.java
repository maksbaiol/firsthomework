public class Sumsung extends Phone implements PhoneConnection,PhoneMedia{
    @Override
    public void dial() {
        System.out.println("Dial number 21312");
    }

    @Override
    public void sendMassage() {
        System.out.println("Sending a massage");
    }

    @Override
    public void makePhoto() {
        System.out.println("Making a photo");
    }

    @Override
    public void makeVideo() {
        System.out.println("Making a video");
    }
}
