public class Lenovo extends Phone implements PhoneMedia,PhoneConnection{
    @Override
    public void dial() {
        System.out.println("Calling!");
    }

    @Override
    public void sendMassage() {
        System.out.println("Sending a massage...");
    }

    @Override
    public void makePhoto() {
        System.out.println("Making a photo of a tree:)");
    }

    @Override
    public void makeVideo() {
        System.out.println("Making a video of different trees:)");
    }
}
