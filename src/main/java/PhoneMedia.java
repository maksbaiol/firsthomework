public interface PhoneMedia {
    void makePhoto();
    void makeVideo();
}
